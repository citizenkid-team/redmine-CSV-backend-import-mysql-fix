-- Import time entries (fixing issues import is needed beforehand).
--
-- WARNING: this script merges imported time entries targeting the
-- same issue at the same day.

START TRANSACTION;

USE 'bitnami_redmine';

DROP TABLE IF EXISTS ap_import_temp;

CREATE TABLE IF NOT EXISTS ap_import_temp
(
project varchar(100),
date varchar(100),
week varchar(100),
user varchar(100),
activity varchar(100),
issue varchar(500),
comment varchar(800),
hours varchar(100)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8
;

-- Note: When you insert into an empty table with INSERT or LOAD DATA,
-- MariaDB automatically does a DISABLE KEYS before and an ENABLE KEYS
-- afterwards.
LOAD DATA INFILE '/tmp/redmine-time-entries-import.csv'
INTO TABLE ap_import_temp
CHARACTER SET 'latin1'
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
IGNORE 1 LINES
;

-- Issue is like 'task #6181: mises en prod'.
UPDATE ap_import_temp
SET issue = IF(
        LOCATE('#', issue) > 0,
        SUBSTRING(
            issue, LOCATE('#', issue) + 1,
            LOCATE(':', issue) - LOCATE('#', issue) - 1),
        issue),
    activity = IF(activity LIKE 'd%veloppement', 'development', activity),
    -- It's best to separate imported time_entries from already
    -- existing time_entries, to limit the risk of messing them up.
    comment = CONCAT(comment, ' --- IMPORTED ---')
;

ALTER TABLE ap_import_temp ADD id INT PRIMARY KEY AUTO_INCREMENT;

-- We need to remove the "duplicates" for the WHERE NOT EXISTS part of
-- the final INSERT.

-- create new time entries corresponding to the sum of "duplicates" for
-- a day.
INSERT INTO ap_import_temp (
project, user, issue, hours, comment, activity, date, week
)
SELECT project, user, issue, ROUND(SUM(hours), 2),
       CONCAT(GROUP_CONCAT(DISTINCT comment SEPARATOR ' ')),
       activity, date, week
FROM ap_import_temp
GROUP BY project, user, issue, activity, date, week
HAVING count(*) > 1
;

-- Delete the "duplicates". max(id) -> The ones just inserted by the
-- query above. So we merge duplicates.
DELETE a
FROM ap_import_temp a
LEFT JOIN (SELECT max(id) AS id, project, user,
                  issue, activity, date, week
           FROM ap_import_temp
           GROUP BY project, user, issue, activity, date, week) b
           ON a.id = b.id AND
              a.project = b.project AND
              a.user = b.user AND
              a.issue = b.issue AND
              a.activity = b.activity AND
              a.date = b.date AND
              a.week = b.week
WHERE b.id IS NULL
;

-- Unique constraint, just for the import. Removed at the end.
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'ap_import_temp' AND
        index_name like 'uniq_time_entry'
    )
    ,'select ''index uniq_fingerprint constraint exists'' _______;'
    , 'alter table `ap_import_temp` add unique uniq_time_entry (project, user, issue(20), activity, date);') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

-- Import data.
INSERT INTO time_entries (
project_id, user_id, issue_id, hours, comments, activity_id,
spent_on, tyear, tmonth, tweek, created_on, updated_on
)
SELECT projects.id, u.id, issues.id, t.hours, t.comment,
       enumerations.id,
       STR_TO_DATE(t.date, '%m/%d/%Y'),
       YEAR(STR_TO_DATE(t.date, '%m/%d/%Y')),
       MONTH(STR_TO_DATE(t.date, '%m/%d/%Y')),
       DAY(STR_TO_DATE(t.date, '%m/%d/%Y')),
       STR_TO_DATE(t.date, '%m/%d/%Y'), -- actual data not exported,
                                        -- lost.
       CURRENT_TIME() -- actual data not exported, lost.
FROM ap_import_temp t
JOIN projects ON
     LOWER(alphanum(projects.name)) = LOWER(alphanum(t.project))
JOIN users u ON
     LOWER(alphanum(CONCAT(u.firstname, u.lastname))) = LOWER(alphanum(t.user))
JOIN issues ON t.issue = issues.id
LEFT JOIN enumerations ON
     LOWER(alphanum(enumerations.name)) = LOWER(alphanum(t.activity)) AND
     enumerations.type = 'TimeEntryActivity'
-- Avoid duplicates.
WHERE NOT EXISTS (
    SELECT id
    FROM time_entries ne
    WHERE
        ne.project_id = projects.id AND
        ne.user_id = u.id AND
        ne.issue_id = issues.id AND
        abs(ne.hours - t.hours) <= 1e-6 AND
        ne.activity_id = enumerations.id AND
        ne.spent_on = STR_TO_DATE(t.date, '%m/%d/%Y')
)
;
-- select (select round(sum(hours), 2)
--         from time_entries) as total_time_entries,
--        (select round(sum(hours), 2)
--         from ap_import_temp) as total_import_data_time_entries;

DROP TABLE IF EXISTS ap_import_temp;

COMMIT;
