-- Fix most flaws of Redmine (very) limited import/export features, at
-- the database level. Useful only if you have a Redmine account
-- somewhere with rights to export to CSV issues and time_entries, but
-- no access to the database... For MariaDB (should work (almost ?) as
-- is in MySQL too).

-- Shouldn't be too difficult to adapt to other RDBMS like
-- PostgreSQL.

-- Note that before running this fix, you should:

-- 1. Make sure projects, trackers, users and enumerations etc
-- referenced in the data exists beforehand. Only categories and
-- target versions can be automatically created by the import.

-- 2. Import CSV file (URL path: /issues/imports/new) using Redmine.
-- Beware of the encoding etc.

-- 3. Change database name and CSV file paths in this script.

-- WARNING: just some quick scripts to fix our situation, we only have
-- 1 project at this time, and we had to import to a fresh Redmine
-- instance. ***Backup before using this***. For exemple the
-- 'fingerprint' used below only takes the first 100 characters of the
-- description of an issue, and it may be insufficient for your data.
-- Also the ids of imported ids are slided, then changed to their
-- correct value. But if you already have existing unrelated issues
-- with this ids, it will fail... etc...

-- -----------------------------------------------
-- http://www.redmine.org/boards/1/topics/15943

-- Rails doesn't impose constraints on databases because it supports
-- lots of different database types. Supporting constraints for so
-- many different database types would be difficult, that's why all
-- constraints are enforced at the application level.

-- MariaDB [bitnami_redmine]> SELECT table_name, TABLE_ROWS FROM
-- INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'bitnami_redmine'
-- and TABLE_ROWS > 0;

-- +-------------------+------------+
-- | table_name        | TABLE_ROWS |
-- +-------------------+------------+
-- | email_addresses   |          2 |
-- | enabled_modules   |         10 |
-- | enumerations      |          9 |
-- | import_items      |       1248 |
-- | imports           |          5 |
-- | issue_categories  |         12 |
-- | issue_statuses    |          6 |
-- | issues            |        260 |
-- | projects_trackers |          5 |
-- | roles             |          4 |
-- | schema_migrations |        253 |
-- | tokens            |          2 |
-- | trackers          |          6 |
-- | user_preferences  |          1 |
-- | users             |          4 |
-- | versions          |          4 |
-- | workflows         |        288 |
-- +-------------------+------------+
-- 17 rows in set (0.00 sec)

-- Fix:
-- issue id (number)
-- issue author
-- issue created_on, updated_on dates
-- issue hierarchy (parent/child)
-- issue status
-- import time entries

-- NO FIX (data is here):
-- issue relations

-- DATA LOST, not exported by Redmine:
-- issues' notes (!!!). Unless you have a custom webscrapper (meh...).
-- time_entries created_on
-- time_entries updated_on

START TRANSACTION;

USE 'bitnami_redmine';

DROP TABLE IF EXISTS ap_import_temp;

-- CSV issues file import table.
CREATE TABLE IF NOT EXISTS ap_import_temp
(
issue_num int(11) NOT NULL,
project varchar(100),
tracker varchar(100),
parent_task varchar(100),
status varchar(100),
priority varchar(100),
subject varchar(255),
author varchar(100),
assignee varchar(100),
updated varchar(50),
category varchar(100),
target_version varchar(100),
start_date varchar(50),
due_date varchar(50),
estimated_time varchar(100),
total_estimated_time varchar(100),
spent_time varchar(100),
total_spent_time varchar(100),
percent_done varchar(100),
created varchar(50),
closed varchar(50),
related_issues varchar(100),
story_points varchar(100),
description varchar(100)
-- PRIMARY KEY (issue_num),
-- UNIQUE INDEX no_dups (issue_num, project)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8
;

-- Note: When you insert into an empty table with INSERT or LOAD DATA,
-- MariaDB automatically does a DISABLE KEYS before and an ENABLE KEYS
-- afterwards.
LOAD DATA INFILE '/tmp/redmine-issues-import.csv'
INTO TABLE ap_import_temp
CHARACTER SET 'latin1'
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
IGNORE 1 LINES
;

-- Probably the last line of the CSV (EOF ?)
DELETE FROM ap_import_temp WHERE issue_num = 0;

-- ALTER TABLE ap_import_temp DROP COLUMN project;
-- ALTER TABLE ap_import_temp DROP COLUMN tracker;
-- ALTER TABLE ap_import_temp DROP COLUMN parent_task;
-- ALTER TABLE ap_import_temp DROP COLUMN status;
ALTER TABLE ap_import_temp DROP COLUMN priority;
-- ALTER TABLE ap_import_temp DROP COLUMN description;
-- ALTER TABLE ap_import_temp DROP COLUMN subject;
-- ALTER TABLE ap_import_temp DROP COLUMN author;
ALTER TABLE ap_import_temp DROP COLUMN assignee;
-- ALTER TABLE ap_import_temp DROP COLUMN updated;
ALTER TABLE ap_import_temp DROP COLUMN category;
-- ALTER TABLE ap_import_temp DROP COLUMN target_version;
ALTER TABLE ap_import_temp DROP COLUMN due_date;
ALTER TABLE ap_import_temp DROP COLUMN start_date;
ALTER TABLE ap_import_temp DROP COLUMN estimated_time;
ALTER TABLE ap_import_temp DROP COLUMN total_estimated_time;
ALTER TABLE ap_import_temp DROP COLUMN spent_time;
ALTER TABLE ap_import_temp DROP COLUMN total_spent_time;
ALTER TABLE ap_import_temp DROP COLUMN percent_done;
-- ALTER TABLE ap_import_temp DROP COLUMN created;
ALTER TABLE ap_import_temp DROP COLUMN closed;
ALTER TABLE ap_import_temp DROP COLUMN related_issues;
ALTER TABLE ap_import_temp DROP COLUMN story_points;

-- Needed to build a old issue num <-> correct issue num mapping.
ALTER TABLE ap_import_temp ADD COLUMN old_issue_num int(11) NOT NULL;

-- alphanum(STR) removes any non-alphanumeric character from STR.
-- [https://stackoverflow.com/a/22903586]
DROP FUNCTION IF EXISTS alphanum;
DELIMITER |
CREATE FUNCTION alphanum( str text ) RETURNS text
BEGIN
  DECLARE i, len SMALLINT DEFAULT 1;
  DECLARE ret CHAR(32) DEFAULT '';
  DECLARE c CHAR(1);
  SET len = CHAR_LENGTH( str );
  REPEAT
    BEGIN
      SET c = MID( str, i, 1 );
      IF c REGEXP '[[:alnum:]]' THEN
        SET ret=CONCAT(ret,c);
      END IF;
      SET i = i + 1;
    END;
  UNTIL i > len END REPEAT;
  RETURN ret;
END |
DELIMITER ;

-- Fingerprint column, if not already here.
SET @preparedStatement = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE  table_name = 'ap_import_temp'
        AND table_schema = DATABASE()
        AND column_name = 'fingerprint'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE `ap_import_temp` ADD `fingerprint` varchar(40);"
));
PREPARE alterIfNotExists FROM @preparedStatement;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Unique fingerprints constraint, if not already here.
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'ap_import_temp' AND
        index_name like 'uniq_fingerprint'
    )
    ,'select ''index uniq_fingerprint constraint exists'' _______;'
    , 'alter table `ap_import_temp` add unique uniq_fingerprint (fingerprint);') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

-- description's index if not already here.
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'ap_import_temp' AND
        index_name like 'desc_100'
    )
    ,'select ''index desc_100 index exists'' _______;'
    ,'create index desc_100 on ap_import_temp(description(100))') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

UPDATE ap_import_temp
SET fingerprint = SHA1(LOWER(
        CONCAT(subject,
               LOWER(alphanum(project)),
               LOWER(target_version),
               LOWER(tracker),
               alphanum(COALESCE(SUBSTRING(description, 1, 100), "")))))
;

ALTER TABLE ap_import_temp ADD INDEX (fingerprint);

-- Fingerprint column, if not already here.
SET @preparedStatement = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE  table_name = 'issues'
        AND table_schema = DATABASE()
        AND column_name = 'fingerprint'
    ) > 0,
    "SELECT 1",
    "ALTER TABLE `issues` ADD `fingerprint` varchar(40);"
));
PREPARE alterIfNotExists FROM @preparedStatement;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Unique fingerprints constraint, if not already here.
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'issues' AND
        index_name like 'uniq_fingerprint'
    )
    ,'select ''index uniq_fingerprint constraint exists'' _______;'
    , 'alter table `issues` add unique uniq_fingerprint (fingerprint);') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

-- description's index if not already here.
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'issues' AND
        index_name like 'desc_100'
    )
    ,'select ''index desc_100 index exists'' _______;'
    ,'create index desc_100 on issues(description(100))') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

UPDATE issues
     LEFT JOIN projects ON project_id = projects.id
     LEFT JOIN versions ON fixed_version_id = versions.id
     LEFT JOIN trackers ON tracker_id = trackers.id
SET fingerprint = SHA1(LOWER(
        CONCAT(COALESCE(issues.subject, ""),
               alphanum(COALESCE(LOWER(projects.name), "")),
               COALESCE(LOWER(versions.name), ""),
               COALESCE(LOWER(trackers.name), ""),
               alphanum(COALESCE(SUBSTRING(issues.description, 1, 100), "")))))
;

-- Should be unique. Abort if not.
ALTER TABLE issues ADD INDEX (fingerprint);

-- Duplicates on subject + description, can't use that alone.
UPDATE ap_import_temp t
JOIN issues ON issues.fingerprint = t.fingerprint
SET t.old_issue_num = issues.id,
    issues.created_on = STR_TO_DATE(t.created, '%m/%d/%Y %l:%i %p'),
    issues.updated_on = STR_TO_DATE(t.updated, '%m/%d/%Y %l:%i %p')
;

-- Store max id from issues...
SELECT @maxid := greatest(max(issue_num), max(id))
FROM issues, ap_import_temp
;

-- Slide ids to prevent duplicates id.
UPDATE issues
JOIN ap_import_temp ON old_issue_num = id
SET id = @maxid + id,
    old_issue_num = @maxid + id
;

-- Fix issues' ids.
UPDATE issues
JOIN ap_import_temp ON old_issue_num = id
SET id = issue_num
;

-- The back-end import also ignores parent tasks. Fix parent -> child
-- relation. Not root_id or lft and rgt values, this is done by the
-- recursive procedure below.
UPDATE issues
JOIN ap_import_temp ON id = issue_num
SET parent_id = parent_task
WHERE parent_task IS NOT NULL and parent_task > 0
;

-- Would need recursive query to get the real root...
-- UPDATE issues
-- JOIN ap_import_temp ON old_issue_num = root_id
-- SET root_id = parent_task
-- WHERE parent_task IS NOT NULL and parent_task > 0
-- ;

-- https://www.redmine.org/boards/2/topics/45051

DROP PROCEDURE IF EXISTS recalculateNestedSet;
DROP PROCEDURE IF EXISTS recalculateNestedSet_recurse;

DELIMITER //

CREATE PROCEDURE recalculateNestedSet()
BEGIN
    -- ensure root_id is correct for roots. Do it quickly here.
    UPDATE issues SET root_id = id WHERE parent_id IS NULL;

    -- MySQL didn't/doesn't allowed OUT or INOUT parameters
    SET @left_value = 1;

    -- now do recusion
    CALL recalculateNestedSet_recurse(NULL, NULL);
END
//

CREATE PROCEDURE recalculateNestedSet_recurse(root INTEGER, parent INTEGER)
BEGIN
    DECLARE done             INTEGER DEFAULT 0;
    DECLARE node             INTEGER;
    DECLARE roots     CURSOR FOR SELECT id FROM issues WHERE parent_id IS NULL  ORDER BY id;
    DECLARE children  CURSOR FOR SELECT id FROM issues WHERE parent_id = parent ORDER BY id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    -- MySQL setting - allow up to 10 stored procedure recursions.
    -- Default is 0.
    SET max_sp_recursion_depth = 10;

    -- this is bypassed on first run
    IF parent IS NOT NULL THEN
        UPDATE issues SET root_id = root, lft = @left_value WHERE id = parent;
        SET @left_value = @left_value + 1;
    END IF;

    OPEN roots;
    OPEN children;

    -- for 1st run, and for root nodes
    IF parent IS NULL THEN
        FETCH roots INTO node;
        REPEAT
        IF node IS NOT NULL THEN
            CALL recalculateNestedSet_recurse(node, node);
            SET @left_value = @left_value + 1;
        END IF;
        FETCH roots INTO node;
        UNTIL done END REPEAT;
    ELSE
        FETCH children INTO node;
        REPEAT
        IF node IS NOT NULL THEN
            CALL recalculateNestedSet_recurse(root, node);
            SET @left_value = @left_value + 1;
        END IF;
        FETCH children INTO node;
        UNTIL done END REPEAT;
END IF;
UPDATE issues set rgt = @left_value where id = parent;

CLOSE roots;
CLOSE children;
END
//
DELIMITER ;

CALL recalculateNestedSet;

-- The back-end import also ignores issue statuses...
UPDATE issues
JOIN ap_import_temp ON id = issue_num
JOIN issue_statuses isss ON isss.name = status
SET status_id = isss.id
;

-- The back-end import also ignores authors (even if user exists
-- before import).
UPDATE issues
JOIN ap_import_temp t ON id = t.issue_num
JOIN users u ON
     LOWER(alphanum(CONCAT(u.firstname, u.lastname))) = LOWER(alphanum(t.author))
SET author_id = u.id
;


-- -------------------------------
-- Cleanups
SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'time_entries' AND
        index_name like 'desc_100'
    )
    ,'select ''index desc_100 index exists'' _______;'
    ,'ALTER TABLE issues DROP INDEX desc_100') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

SELECT if (
    EXISTS(
        SELECT DISTINCT index_name from information_schema.statistics
        WHERE table_schema = 'bitnami_redmine' AND
        table_name = 'time_entries' AND
        index_name like 'desc_100'
    )
    ,'select ''index desc_100 index exists'' _______;'
    ,'ALTER TABLE issues DROP INDEX uniq_fingerprint') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

SET @preparedStatement = (SELECT IF(
    (SELECT COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE  table_name = 'issues'
        AND table_schema = DATABASE()
        AND column_name = 'fingerprint'
    ) < 1,
    "SELECT 1",
    "ALTER TABLE issues DROP COLUMN fingerprint;"
));
PREPARE alterIfNotExists FROM @preparedStatement;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

DROP TABLE IF EXISTS ap_import_temp;

COMMIT;
